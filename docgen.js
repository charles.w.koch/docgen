
var config;
var data;
const basestyle = `
body {
	margin: 0;
}

div {
	overflow: hidden;
	box-sizing: border-box;
}

.US-Letter {
	height: 10.97in;
	width: 8.5in;
	padding: 0.25in;
	display: flex;
	flex-wrap: wrap;
	align-content: flex-start;
	page-break-after: always;
    justify-content: center;
}

.US-Letter-Landscape {
	height: 8.5in;
	width: 11in;
	padding: 0.25in;
	display: flex;
	flex-wrap: wrap;
	align-content: flex-start;
	page-break-after: always;
	justify-content: center;
}
`;

window.onload = function()
{
	configLoader = document.getElementById('config_file');
	configLoader.addEventListener('change', loadConfig);

	dataLoader = document.getElementById('data_file');
	dataLoader.addEventListener('change', loadData);
}


function loadConfig(file) {
	let cfile = configLoader.files[0];
	let creader = new FileReader();
	creader.onload = function(){readConfig(creader.result)}
	creader.readAsText(cfile);
}

function readConfig(incoming) {
	console.log('Config successfully loaded');
	let cfileLines = incoming.split("\n");
	config = {
		"stylesheet": "",
		"template": "",
		"page_type": "US-Letter",
		"templates_per_page": 1,
		"doc_title": "Generated Document"
	};
	curr_field = "";
	for(line of cfileLines) {
		if( line.includes("stylesheet =") ) {
			curr_field = "stylesheet";
		} else if( line.includes("template =") ) {
			curr_field = "template";
		} else if ( line.includes("page_type = ") ) {
			config.page_type = line.slice(12);
			curr_field = "";
		} else if ( line.includes("templates_per_page = ") ) {
			config.templates_per_page = parseInt(line.slice(21));
			curr_field = "";
		} else if ( line.includes("doc_title = ") ) {
			config.doc_title = line.slice(12)
			curr_field = "";
		} else if ( line.includes("'''") ) {
			curr_field = "";
		} else {
			if( curr_field != "" ) {
				config[curr_field] += line + "\n";
			}
		}
	}
}

function loadData(file) {
	let dfile = dataLoader.files[0];
	let dreader = new FileReader();
	dreader.onload = function(){readData(dreader.result)}
	dreader.readAsText(dfile);
}

function readData(incoming) {
	console.log('Data successfully loaded');
	data = d3.csvParse(incoming);
}

function render() {
	if( config === undefined || config == null ) {
		console.error('Missing config file');
	}
	if( data === undefined || data == null ) {
		console.error('Missing data file');
	}

	let outstr = `<html>
<head>
<title>`
	outstr += config.doc_title;
	outstr += `</title>
<style>`;
	outstr += basestyle;
	outstr += config.stylesheet;
	outstr += `</style>
</head>
<body>`;

	let records = 0;
	let newpage = true;
	for ( var i = 0; i < data.length; i++ ) {
		records += 1;

		if ( newpage ) {
			newpage = false;
			outstr += '<div class="' + config.page_type + '">\n';
		}

		let obj = data[i];
		let temp = config.template;
		for( field in obj ) {
			let fieldstr = "{ " + field + " }";
			temp = temp.replaceAll(fieldstr, obj[field]);
		}
		outstr += "\n";
		outstr += temp;
		outstr += "\n";

		if( records >= config.templates_per_page ) {
			outstr += "</div>\n";
			records = 0;
			newpage = true;
		}
	}
	if(! newpage) {
		outstr += "</div>\n";
	}
	outstr += `</body>
</html>`;

	return outstr;
}


function renderAndGo() {
	let newwindow = window.open();
	let newdocument = newwindow.document;
	newdocument.write(render());
	newdocument.close();
}


