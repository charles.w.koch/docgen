# DocGen

DocGen (Document Generator) is a utility to convert spreadsheet data into a beautiful, multi-page document. Originally developed for creating playing cards, DocGen can be leveraged to create any kind of document from multiple data records, such as mailing address labels, table placards, personnel files, and much, much more.

The original DocGen was a command line utility written in Python, but to make it slightly more user friendly, it has been updated to be displayed and executed in a browser via HTML and JavaScript.

## Installation

Simply add the ```docgen``` directory to any server's file structure and point your browser to it's location. If placed at the root of the public HTML directory, it will be accessed via the following address: ```http://<server or domain>/docgen/docgen.html```

## Usage

The DocGen UI requires 2 files that must be read into the browser to generate a document. The **config** file is a text file that defines the make up of the document, and a **data** file, which contains the data DocGen uses to populate the document. Both of these files must be imported to DocGen before hitting the **Generate**, button, which will read both of the files and then open a new window with the document, which can then be printed.

### Config file

The config file is a text file that contains several parameters for laying out the document and indicating where the data lives within the document. The document generated will be an HTML page formatted specifically for printing. As such, most of the config file is made of HTML and CSS with a few extra parameters to determine the layout.

```stylesheet =```

The stylesheet is literally just a collection of CSS that styles the document. This includes all of the styles and formatting necessary for the template (see below).

```template =```

The template is a section of HTML that defines the structure of a single data record. For example, if creating a deck of playing cards, the template is a single card, with all of the fields and sections defined within it. To place data in the template, put the name of a data field within curly braces, with a space on either side of the name like so: ```{ Card Name }```. This can be done anywhere in the template, so any part can include data fields, including CSS class names, inline CSS, HTML attributes, HTML tags, etc. It is a literal text replacement so you can get quite creative with how the template reacts to your data file!

```page_type = US-Letter```

The page type tells DocGen what size paper to prepare the document for printing. DocGen only includes 2 page types by default: ```US-Letter``` and ```US-Letter-Landscape```. However, if a different paper size is desired, simply define a specific class for your page in your stylesheet with the following setup:

```
.My-Page-Type {
	height: <height>in ;
	width: <width>in;
	padding: <margin>in;
	display: flex;
	flex-wrap: wrap;
	align-content: flex-start;
	page-break-after: always;
}
```

You may also define the dimensions in centimeters (cm) or millimeters (mm) if you prefer.

```templates-per-page = 9```

This setting tells DocGen how many records should fit on a single page. To ensure no issues with the layout, make sure that the total dimensions of your templates do not exceed the size of your ```page_type```. If you are using the default ```US-Letter``` or ```US-Letter-Landscape``` pages, DocGen will try to fit as many records as possible horizontally before moving down to the next row.

### Data File

The data file is a comma-separated values (CSV) file, a format commonly available from spreadsheet programs and other data management programs. The first row of the data file contains the name of each field, and each subsequent row is a record of the dataset. The field names in the data file must match the field names used in the ```template =``` of the config file for DocGen to associate the two. Additional fields names that are not used in the template are okay, but fields that are missing from the data that are included in the template will cause visible problems on the document itself.

## Generating the Document

Once you have both the config and data files loaded into DocGen, you can click the **Generate** button. This will open a new window with the generated document. Printing the document is the same as printing a page directly from your browser. For most computers, this is done by right-click and selecting "Print".

**When printing, make sure that your printer is set with No Margins.** DocGen's default page types include a 0.25 inch margin in the layout. If your printer inserts its own margins, it can change the size of the resulting document. Many OS's and browsers simply scale the document down to insert a margin. To prevent this, you must change your printer settings to have no margins.

